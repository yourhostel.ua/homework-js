"use strict";
// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

// * Не всі мобільні пристрої генерують події keypress/keydown.
// * якщо в input вставити текст за допомогою виклику контекстного меню миші жодної події не відбудеться.

function switch1(){
    document.querySelector('body').addEventListener('keydown',(e) => {
        document.querySelectorAll('.btn').forEach((element)=>{
            element.classList.remove('active')
        })
        const act = {
             w:(a) => {document.querySelector(`button:nth-child(${a})`).classList.add('active')},
            'Enter': () => {act.w(1);},
            'KeyS': () => {act.w(2); },
            'KeyE': () => {act.w(3); },
            'KeyO': () => {act.w(4); },
            'KeyN': () => {act.w(5); },
            'KeyL': () => {act.w(6); },
            'KeyZ': () => {act.w(7); },
        };
        act[e.code]()
    })
}

switch1()

//*********************************** другий варіант *******************************************************************

// function switch2() {
//     document.querySelector('body').addEventListener('keyup', (e) => {
//         document.querySelectorAll('.btn').forEach((element) => {
//             element.classList.remove('active')
//         })
//         const w = (a) => {document.querySelector(`button:nth-child(${a})`).classList.add('active')};
//         switch (e.code) {
//             case ('Enter'):
//                 return w(1);
//             case ('KeyS'):
//                 return w(2);
//             case ('KeyE'):
//                 return w(3);
//             case ('KeyO'):
//                 return w(4);
//             case ('KeyN'):
//                 return w(5);
//             case ('KeyL'):
//                 return w(6);
//             case ('KeyZ'):
//                 return w(7);
//         }
//     })
// }
//
// switch2()
