'use strict';
/*   Теоретичні питання
*
*       Опишіть своїми словами що таке Document Object Model (DOM)
*       Яка різниця між властивостями HTML-елементів innerHTML та innerText?
*       Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
*
*       Завдання
*       Код для завдань лежить в папці project.
*
*   Знайти всі параграфи на сторінці та встановити колір фону #ff0000
*   Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
*   Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph
*   Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
*   Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
*
*       Література: https://uk.javascript.info/searching-elements-dom
*/

// 1. Це інтерфейс, своєрідна панель зі своєрідними "перемикачами", за допомогою яких інша програма може впливати, керувати вмістом HTML.
// 2. Рядок з HTML тегами, за допомогою властивості innerText вставляється як текст *set, (зчитується лише текст *get).
//    На відміну від innerText, за допомогою innerHTML, після вставки, теги спрацюють *set, (зчитуються і теги теж *get)

console.log('************* теорія ****************')
//  наприклад встановити (set):

// <div class = "test"></div>
 const element = document.querySelector('.test');
 element.innerHTML = '<a href = "#">link</a>' ;  // У вікно браузера виведе підкреслений синій link (тег <а> спрацює)
 // element.innerText = '<a href = "#">link</a>'   //а тут виведе у вигляді тексту  <a href = "#">link</a> (крім того є альтернатива innerText це - textContent він побачить навіть те, що всередині тега закрито стилем "display: none")

//  Якщо зчитувати (get):

// <div class = "test2">
//     <a href="#">link</a>
// </div>
 const element2 = document.querySelector('.test2');
 console.log(`${element2.innerHTML} \t це - "innerHTML"`); // у консоль виведе => <a href="#">link</a>
 console.log(`${element2.innerText} \n\t це - "innerText"`); // а тут => link
//
//
//    3. Кращими способами вважатимуться такі методи:
//                              document.querySelectorAll('.className, #id, span')
//                              document.querySelector('.className')
//                              document.getElementById('id')
//    Також можна використовувати більш застарілі :
//                              document.getElementsByName('name')
//                              document.getElementsByTagName('div')
//                              document.getElementsByClassName('className')
//    за допомогою цих можна створювати так звані "живі колекції" тому їх теж не треба скидати з рахунків

//***********************************************************************************************

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

for(let element of document.querySelectorAll('p')){
    element.style.backgroundColor = '#ff0000';
}

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

console.log('************* елемент із id="optionsList" ****************');

   console.log(document.querySelector('#optionsList'));               // елемент із id="optionsList"

console.log('************* батьківський елемент ****************');

   console.log(document.querySelector('#optionsList').parentElement); // батьківський елемент

console.log('************* дочірні ноди ****************');

   const nodes = document.querySelector('#optionsList').childNodes;    // дочірні ноди
   for (let i = 0; i < nodes.length; i++){
       console.log('nodeName: ' + nodes[i].nodeName + ',  nodeType: ' + nodes[i].nodeType);
   }

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph

const newParagraph = document.createElement('p');                          //перший варіант
      document.querySelector('.section-options').append(newParagraph);
      newParagraph.classList.add('testParagraph');
      newParagraph.innerText = 'This is a paragraph';
                                                                                   //другий варіант, результат той же
// document.querySelector('.section-options').insertAdjacentHTML("beforeend", '<p class="testParagraph">This is a paragraph</p>')

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

console.log('*** елементи, вкладені в main-header=>новий клас nav-item ***');

    const mainHeaderElements = document.querySelector('.main-header').children;
    for(let element of mainHeaderElements){
        element.classList.add('nav-item');
        console.log(element);
    }

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitleElements = document.querySelectorAll('.section-title');
sectionTitleElements.forEach(element => element.classList.remove('section-title'));
