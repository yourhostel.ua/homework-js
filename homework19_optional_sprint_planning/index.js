'use strict';

function sprintPlanning(personPoints,tasksPoints,deadline){
  function workingHours(deadline) {
     const today = new Date(),                    //сьогодні
           checkedDay = new Date();             //день тижня що перевіряється
       let term = new Date(deadline),
           j =  (term.getTime() - today.getTime())/(1000*60*60*24), //період
           dayOff = 0                             //вихідні
       for (let i = 0; i <= j; i++) {
           checkedDay.setTime(today.getTime()+(1000*60*60*24)*i);
        if ((checkedDay.getDay() === 6) || (checkedDay.getDay() === 0)) { //6 = субота, 0 = неділя
                dayOff++;               //якщо вихідні збільшуємо їх змінну
        }
       }
     return Math.floor((j - dayOff)*8);            //кількість робочих годин до кінця дедлайну
  }
       let totalPointsPerson  = personPoints.reduce((value,item) => item + value ), //командні 'story points' за один робочий день
           totalTasks = tasksPoints.reduce((value,item) => item + value ), //'story points' необхідні для виконання поставлених завдань
           neededHours = Math.floor(totalTasks/totalPointsPerson*8),     //скільки робочих годин потрібно
           deadlineHours = workingHours(deadline);  //скільки годин є до кінця дедлайну
           console.log(neededHours)
        if (neededHours > deadlineHours){
               alert(`Команді розробників доведеться витратити додатково ${neededHours - deadlineHours} годин після дедлайну, щоб виконати всі завдання в беклозі`)
         } else {
               alert(`Усі завдання будуть успішно виконані за ${Math.ceil(Math.abs((neededHours - deadlineHours)/8))} днів до настання дедлайну!`)
           }

}

sprintPlanning([65,100,85,55],[200,200,80,115,800],'2022/09/20')
