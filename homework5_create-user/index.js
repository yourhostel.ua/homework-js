'use strict';
// Опишіть своїми словами, що таке метод об'єкту
// Який тип даних може мати значення властивості об'єкта?
// Об'єкт це посилальний тип даних. Що означає це поняття?


// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// Технічні вимоги:
//
//     Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
//
//Необов'язкове завдання підвищеної складності
//
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
//
//Література: https://learn.javascript.ru/object https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty

// 1. метод об'єкта – функція як властивість об'єкта
// 2. Будь-яке, примітив інший об'єкт... не може не мати значення
// 3. При присвоєнні змінної передається посилання, а не сам об'єкт

//************************************** createNewUser ****************************************************************

function createNewUser() {                               //створюємо функцію
    const newUser = {                                    //всередині функції створюємо об'єкт
        firstName: undefined,                                    // із властивостями firstName та lastName
        lastName: undefined,                                     //значення яких невизначено
        setAskBlock() {                                         //Створюємо сеттер-опитувач. Блокуємо властивості
            Object.defineProperty(this, 'firstName',
                {
                    value: prompt('Enter your first name'), //питаємо ім'я привласнюємо firstName
                    writable: false,          //блокуємо зміну властивості(тільки зчитування)
                    configurable: true,       //дозволяємо видалення властивості, якщо false блокує запис і видалення, властивість стає недоступною для Object.defineProperty
                    enumerable: true          //дозволяємо читання циклом
                }
            );
            Object.defineProperty(this, 'lastName',
                {
                    value: prompt('Enter your last name'), //питаємо прізвище привласнюємо lastName
                    writable: false,       //блокуємо зміну властивості, тільки зчитування
                    configurable: true,    //також дозволяємо видалення і зміну через Object.defineProperty
                    enumerable: false      //блокуємо читання циклом
                }
            )
        },
        setFirstName(newFName) {                          //створюємо сеттер для FirstName
            Object.defineProperty(this, 'lastName',
                {
                    value: newFName
                }
            )
        },
        setLastName(newLName) {                           //створюємо сеттер для LastName
            Object.defineProperty(this, 'firstName',
                {
                    value: newLName
                }
            )
        },
        getLogin() {                                                          // створюємо функцію для відділення першої літери
        return (this.firstName.substring(0, 1) + this.lastName).toLowerCase()  // з імені (функ.substring) та конкатенуємо(+) до прізвища.
    }                                                                          // Опускаємо всі літери в нижній регістр (toLowerCase)
    }
    newUser.setAskBlock();
    return newUser
}

//************************************ end createNewUser ***************************************************************

const newUser = createNewUser()                                 // передаємо об'єкт створений функцією змінної newUser


//************************************** getLogin **********************************************************************

// function login() {                                                         // створюємо функцію для відділення першої літери
//     return (this.firstName.substring(0, 1) + this.lastName).toLowerCase()  // з імені (функ.substring) та конкатенуємо(+) до прізвища.
// }                                                                          // Опускаємо всі літери в нижній регістр (toLowerCase)

console.log(`${(newUser.getLogin())} - Ваш логін створено `)

                     //******************* універсальний варіант *****************

//  function login(a,b) {                                   //можемо використовувати функцію зі змінними, наприклад, з іншого об'єкта
//      return (a.substring(0, 1) + b).toLowerCase()
//  }
//
// newUser.getLogin = login;                                                // Додаємо метод до нашого нового об'єкту
//
// console.log(`${(newUser.getLogin(newUser.firstName, newUser.lastName))} - Ваш логін створено `);

                    //************** end універсальний варіант *****************

//********************************** end getLogin **********************************************************************

//********************************** перевірки *************************************************************************

// delete newUser.firstName;                 //якщо змінимо на  configurable: false для firstName видалення буде неможливим (помилка ' "firstName" is non-configurable and can't be deleted ')
// newUser.firstName = 'Serhiy';             //зміна неможлива (помилка ' Uncaught TypeError: "firstName" is read-only ' )
// newUser.lastName = 'Tyshchenko';          //зміна неможлива (помилка ' Uncaught TypeError: "lastName" is read-only ' )
for (let key in newUser) {                   //не прочитає ті властивості, у яких enumerable: false (зараз це lastName)
    console.log(key)
}

newUser.setFirstName('Vasia');
newUser.setLastName('Pupkin')
console.log(newUser)                         //все записує за допомогою сетерів






























