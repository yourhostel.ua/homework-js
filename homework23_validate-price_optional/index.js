'use strict'
let a = ".shopping-list"
const newGoodInput = document.querySelector('#new-good-input')
const addBtn = document.querySelector('#add-btn')
const shoppingList = document.querySelector(`${a}`)
let rem

addBtn.addEventListener('click',(ev) =>{
    const liSet = document.createElement('li')
    shoppingList.appendChild(liSet).textContent = newGoodInput.value
    const button = document.createElement('button')
    liSet.classList.add('remLi')
    button.classList.add('remButton')
    button.style.cursor = "pointer"
    liSet.style.cursor = "pointer"
    liSet.appendChild(button).textContent = ' x'
    document.querySelectorAll('.remButton').forEach(element => {
        element.addEventListener('click', () => {
            console.log(element)
            element.parentElement.remove()
        })
    })
    document.querySelectorAll('.remLi').forEach(element => {
        element.addEventListener('click', () => {
            element.style.textDecoration = 'line-through'
        })
    })
})