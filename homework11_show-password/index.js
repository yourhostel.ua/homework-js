"use strict";
const inn = document.getElementById('inn'), //введення пароля <input>
    rp = document.getElementById('rp'),   //повторення введення пароля <input>
    btn = document.querySelector('.btn'),  //кнопка форми
    fas_inn = document.querySelector('.fas_inn'),  //око введення пароля <i>
    fas_rp = document.querySelector('.fas_rp'), //око повторення <i>
    blindAlert = document.querySelector('.blind-alert'), //создал свой Alert
    clearTab = () => {                 //блокуємо фокус кнопкою Tab
        inn.tabIndex = -1
        rp.tabIndex = -1
        btn.tabIndex = -1
    },
    setTab = () => {                   //розблокуємо
        inn.tabIndex = 1
        rp.tabIndex = 2
        btn.tabIndex = 3
    }

//******************************************** перемикаємо картинки показуємо пароль ***********************************

function hiding(favicon, inp) {
    favicon.addEventListener('click', (e) => {
        if (e && favicon.classList.contains('fa-eye-slash')) {
            inp.setAttribute('type', 'text');
            favicon.classList.remove('fa-eye-slash');
            favicon.classList.add('fa-eye')
        } else {
            inp.setAttribute('type', 'password');
            favicon.classList.remove('fa-eye');
            favicon.classList.add('fa-eye-slash')
        }
    })
}

hiding(fas_inn, inn)
hiding(fas_rp, rp)

//********************************* робимо функцію myAlert *************************************************************

function myAlert(mass, a = true) {
    blindAlert.style.display = 'block';
    blindAlert.querySelector('p').innerText = mass
    clearTab()
    if (a) {
        document.querySelector('.bt').onclick = () => {
            blindAlert.style.display = 'none';
            inn.value = '';                                 //якщо другий параметр функції true тут очищаємо поле input,
            rp.value = '';
            setTab()
        };                                                     //інакше продовжуємо вчитися вводити пароль
    } else document.querySelector('.bt').onclick = () => {
        blindAlert.style.display = 'none'
        setTab()
    }
}

//*********************************************** перевіряємо однакові паролі чи ні ************************************

document.querySelector('.btn').addEventListener('click', () => {
    if (!inn.value) {
        myAlert(`Потрібно ввести значення " Ввести пароль "`, false)
    } else if (inn.value === rp.value) {
        myAlert(`You are welcome`)
    } else myAlert(`Потрібно ввести однакові значення`, false)
})



