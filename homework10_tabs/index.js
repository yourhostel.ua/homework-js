"use strict";
/*
*  Завдання
*  Реалізувати перемикання вкладок (таби) на чистому Javascript.
*
*      Технічні вимоги:
*
*      У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
*      Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
*      Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
*
*
*      Література: https://developer.mozilla.org/ru/docs/Learn/HTML/Howto/Use_data_attributes
*
*/

//нижче три робочі варіанти .Перший варіант без можливості додавання контенту не коригуючи js

//************************** фінальна версія ******************************************************************************
const cube = document.querySelector('.cube')
let attribute
document.querySelector('.tabs').onclick = function (event) {
    attribute = event.target.getAttribute('data-switch')
    document.querySelectorAll('li').forEach(element => element.classList.remove('active')) //видаляємо клас у кнопках та контенті active
    event.target.classList.add('active')                                  //додаємо активи на нові позиції
    document.getElementById(attribute).classList.add('active')    //зчіплюємо кнопку навігатора з контентом // зчитуємо з поточного елемента data - атрибут, передаємо його як параметр для пошуку елемента контента  по id і чіпляємо на цей елемент клас active
    if (attribute === 'switch1') cube.style.transform = 'rotateY(0deg) rotateX(0deg)';
    else if (attribute === 'switch2') cube.style.transform = 'rotateY(-90deg) rotateX(0deg)';
    else if (attribute === 'switch3') cube.style.transform = 'rotateY(180deg) rotateX(0deg)';
    else if (attribute === 'switch4') cube.style.transform = 'rotateY(90deg) rotateX(0deg)';
    else if (attribute === 'switch5') cube.style.transform = 'rotateY(0deg) rotateX(-90deg)';
    else if (attribute === 'switch6') cube.style.transform = 'rotateY(0deg) rotateX(90deg)';
}

//************************** друга спроба ******************************************************************************

// const tabs = () => {
//     document.querySelectorAll('.tabs-title').forEach(element => {
//         element.addEventListener('click', (event) => {            //чіпляємо прослуховування до кожного елемента .tabs-title
//             const dataAttribute = event.target.getAttribute('data-switch')
//             document.querySelectorAll('li').forEach(element => {element.classList.remove('active')})       //видаляємо активи
//             element.classList.add('active')                                              //додаємо активи на нові позиції
//             document.getElementById(dataAttribute).classList.add('active')      //зчіплюємо кнопку навігатора з контентом
//         })
//
//     })
//     document.querySelector('.tabs-title+li:nth-child(2)').click() //можна заздалегідь не встановлювати клас, просто зробити авто клік
// }
// tabs()

//************************** перша спроба ******************************************************************************

// const tabs = () => {
// const tabsTitle = document.querySelectorAll('.tabs-title')
// const tab = document.querySelector('.tabs')
// const contentSwitch = document.querySelectorAll('.content-switch')
//
// function removeActive(a) {
//     for (let i of a) {
//         i.classList.remove('active');
//     }
// }
// tab.addEventListener('click', (event) => {
//     removeActive(tabsTitle)
//     removeActive(contentSwitch)
//     if (event.target.closest('[data-switch=switch1]')) {
//         document.querySelector('[data-switch=switch1]').classList.add('active')
//         document.querySelector('#switch1').classList.add('active')
//     }
//     if (event.target.closest('[data-switch=switch2]')) {
//         document.querySelector('[data-switch=switch2]').classList.add('active')
//         document.querySelector('#switch2').classList.add('active')
//     }
//     if (event.target.closest('[data-switch=switch3]')) {
//         document.querySelector('[data-switch=switch3]').classList.add('active')
//         document.querySelector('#switch3').classList.add('active')
//     }
//     if (event.target.closest('[data-switch=switch4]')) {
//         document.querySelector('[data-switch=switch4]').classList.add('active')
//         document.querySelector('#switch4').classList.add('active')
//     }
//     if (event.target.closest('[data-switch=switch5]')) {
//         document.querySelector('[data-switch=switch5]').classList.add('active')
//         document.querySelector('#switch5').classList.add('active')
//     }
// })
// }
//
// tabs()


let rotateY = 0, rotateX = 0;
document.addEventListener('keydown', (event) =>  {
                if (event.code === 'ArrowRight')        {rotateY -= 4
                } else if (event.code === 'ArrowUp') {rotateX += 4
                } else if (event.code === 'ArrowLeft') {rotateY += 4
                } else if (event.code === 'ArrowDown') {rotateX -= 4
                }

                document.querySelector('.cube').style.transform =
                    'rotateY(' + rotateY + 'deg)'+
                    'rotateX(' + rotateX + 'deg)';
            })

let setWidth = (document.querySelector('body').offsetWidth) - 300 / 2,
    setHeight = (document.querySelector('body').offsetHeight) - 300 / 2;

const start = () => {
    cube.addEventListener('mousemove', (event) => {
        rotateY = event.pageX - setWidth
        rotateX = event.pageY - setHeight
        document.querySelector('.cube').style.transform =
            'rotateY(' + rotateY + 'deg)' +
            'rotateX(' + rotateX + 'deg)';
    })
}
    start()

















