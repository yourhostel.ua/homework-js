'use strict'
/*
*     Теоретичні питання
*     Опишіть, як можна створити новий HTML тег на сторінці.
*         Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
*         Як можна видалити елемент зі сторінки?
*         Завдання
*         Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
*
*         Технічні вимоги:
*        1 Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
*                                                     до якого буде прикріплений список (по дефолту має бути document.body.
*        2 кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
*                                      Приклади масивів, які можна виводити на екран:
*                                                    ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
*                                                           ["1", "2", "3", "sea", "user", 23];
*                                                                 Можна взяти будь-який інший масив.
*         Необов'язкове завдання підвищеної складності:
*        3 Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив,
*                                                              виводити його як вкладений список. Приклад такого масиву:
*
*                                         ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
*                                   Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
*
*         Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
*
*   Література:https://uk.javascript.info/searching-elements-dom   https://uk.javascript.info/modifying-document
*              http://learn.javascript.ru/es-string                https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map
*              https://learn.javascript.ru/settimeout-setinterval
*/
//********************************** Теоретичні питання ****************************************************************

//   1. innerHTML, outerHTML, createElement('div') перші два можуть додавати текст з тегами і теги спрацюють,
//       останній створює тег, але потім його ще треба додати за допомогою: after, before, prepend, append, appendChild, insertBefore
//         перші сучасні просунуті можуть вставляти відразу кілька елементів і текст одночасно,
//           проте останні хоч і застаріли здатни повернути встановлений елемент, це досить зручно в деяких випадках. Ще для вставки можна використовувати insertAdjacentElement
//   2. Позиція вставки. 4 варіанти перед елементом (beforebegin), усередині перед контентом(afterbegin) перед першим нащадком, усередині після контенту(beforeend) після останнього нащадка, після елемента(afterend).
//
//   3. removeChild повертає віддалений елемент, remove() неповертає.


const container = document.createElement('div')      //створюємо div з класом container
document.body.prepend(container)                            //можна запулити прямо в body. Менюшка, що створюється, просто зміститься в право.
container.classList.add('container')                        //для контейнера прописані стилі центруючі меню по центру

//*************************************************** 1 *******************************************************************
function createList(nod, arrayProto, s = 3) {
    const ulMenu = document.createElement('ul')
    nod.appendChild(ulMenu).classList.add('menu')

    let array

    function List(arrayProto) {
        array = arrayProto.map((key, index, array) => {
            switch (true) {
                case (!Array.isArray(key) && !Array.isArray(array[index + 1])):       //якщо цей і наступний елемент не масив
                    return "<li><a href = '#'>" + key + "</a>"                               //додаємо цей елемент в новий масив
                case ((Array.isArray(array[index + 1]))):                                  //якщо наступний елемент масив
                    return "<li><a href = '#'>" + key + "</a><ul>" + `${List(array[index + 1])}` + "</ul></li>"//вставляємо поточний елемент (key) у якості корневого, а в ньому рекурсивно розгортаємо наступний елемент, який є масивом
            }
        })
        return array //повертаємо наш створений масив
    }

    List(arrayProto) //запускаємо функцію
    //конвертуємо в рядок чистимо від ком і запулюємо в ul - ку
    ulMenu.insertAdjacentHTML('afterbegin', array.join('').replaceAll(",", ""))

    function clear() {
        document.querySelector('.menu').remove()
    }

    function sec(i) {
        const element = document.querySelector('.demo')
        let idTimeout = setTimeout(function () {
            sec(i - 1);
            element.innerHTML = i + ' sec';
            if (!i) {
                element.innerHTML = 'Bye Bye!';
                clear();
                document.querySelector('.demo').style.background = "rgb(67, 29, 29, 0.5)"
            } else {
                document.querySelector('.demo').style.background = "rgb(67, 29, 29, 0.8)"
            }
        }, 1000)
        if (i < 0) clearTimeout(idTimeout)
    }
    sec(s)
}

//**************************************************** 1 end ***********************************************************

//**************************************************** 2 ***************************************************************
// function createList(nod, arrayProto, s = 3) {      //створюємо функцію з трьома параметрами nod - елемент на сторінці всередину якого хочемо помістити список.
//     const ulMenu = document.createElement('ul')    //arrayProto - масив який розвертатимемо. s = час у секундах після якого список буде видалено
//     nod.appendChild(ulMenu).classList.add('menu')           //створюємо кореневу ul - ку яку видалятимемо таймером
//
//     function list(ulMenu, arrayProto){
//         let liSet                                     //li - ка яка використовується в обох умовах
//         arrayProto.forEach((element) => {                    //ітеруємо елементи масиву крім тих чиї значення не задані, функція колбек створює дерево тегів
//             if (Array.isArray(element)) {                   //якщо елемент масив беремо батька (li - шку)створеного в else перед цим та танцюємо від нього
//                 let ul = liSet.appendChild(document.createElement('ul'))    //вкладаємо ul - ку в li - шку
//                 list(ul, element)                                     //викликаємо функцію рекурсивно, передаємо в неї нашу щойно створену ul - ку та елемент який зараз у нас підмасив
//             } else {
//                 liSet = ulMenu.appendChild(document.createElement('li'))       //тут просто створюємо li - шку
//                 let a = document.createElement('a')
//                 a.setAttribute('href', '#')                  // і в li - шку створюємо та додаємо посилання
//                 liSet.appendChild(a).append(element)                               //ну й елемент апендім в посилання
//             }
//         })
//     }
//     list(ulMenu, arrayProto)                                             //ну й запускаємо з батьківською функцією
//
//     function clear() {                                                    //робимо функцію видалення кореневої ul - ки
//         document.querySelector('.menu').remove()
//     }
//
// function sec(i) {                                                     //робимо таймер
//         const element = document.querySelector('.demo')
//         let idTimeout = setTimeout(function () {               //використовуємо setTimeout с тimeout 1000 мс
//             sec(i - 1);                                            //рекурсивно викликаємо кожну секунду, та від i віднімаємо одиницю
//             element.innerHTML = i + ' sec';                           // щоразу оновлюючи результат роботи функції в div.demo
//             if (!i) {
//                 element.innerHTML = 'Bye Bye!';                      // як що 0 Bye Bye!
//                 clear();                                              // та викликаємо функцію видалення списку
//                 document.querySelector('.demo').style.background = "rgb(67, 29, 29, 0.5)" //та змінюємо фон
//             } else {
//                 document.querySelector('.demo').style.background = "rgb(67, 29, 29, 0.8)" // фон коли час цокає
//             }
//         }, 1000)
//         if (i < 0) clearTimeout(idTimeout)                                  //очищаємо Timeout
//     }
//     sec(s) //та запускаємо з основною функцією
// }

//**************************************************** 2end ************************************************************

// ```const container = document.createElement('div')
// document.body.prepend(container)
// container.classList.add('container')
//
// function createList(nod, arrayProto) {
//     const ulMenu = document.createElement('ul')
//     nod.appendChild(ulMenu).classList.add('menu')
//     let array
//     function List(arrayProto) {
//         array = arrayProto.map((key, index, array) => {
//             switch (true) {
//                 case (!Array.isArray(key) && !Array.isArray(array[index + 1])):       //якщо цей і наступний елемент не масив
//                     return "<li><a href = '#'>" + key + "</a>"                               //додаємо цей елемент в новий масив
//                 case ((Array.isArray(array[index + 1]))):                                  //якщо наступний елемент масив
//                     return "<li><a href = '#'>" + key + "</a><ul>" + `${List(array[index + 1])}` + "</ul></li>"//рекурсивно розгортаємо наступний елемент в корінь поточного
//             }
//         })
//         return array
//     }
//     List(arrayProto)
//     ulMenu.insertAdjacentHTML('afterbegin', array.join('').replaceAll(",", ""))//конвертуємо в рядок чистимо від ком і запулюємо в ul - ку
// }
// let arr1 = ["Kharkiv", "Kiev", ["Borispol", ["street"], "Irpin"], "Odessa",["street"] , "Lviv", "Dnieper"]
//
// createList(container, arr1)
// //масив повинен бути структурований так щоб перед кожним підмасивом був хоча б один елемент не масив так як він береться як батьківський для підмасиву```

//**********************************************************************************************************************
//____________два рівні вкладеності(зараз)______________________

let arr1_1 = ["item1", ['subItem1', 'subItem2', 'subItem3', 'subItem4'], "item2", ['subItem5', 'subItem6'], "item3", ['subItem7'], "item4", ['subItem8', 'subItem9', 'subItem10', 'subItem11', 'subItem12']];

//____________три рівні вкладеності теж працюють___________але треба відключити в index.html ресетник і стилі CSS. Стилі лише до другого рівня прописані

let arr1_2 = ["item1", ['subItem1', 'subItem2', 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']];

//я так розумію, він може розгортатися до нескінченності, поки стек не закінчиться. цей теж розклав

let arrBigger = ["item0", ["item1", ['subItem1', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", ["item0", ["item1", ['subItem1', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", ["item1", ['subItem1', 'subItem2', 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']], "Lviv", "Dnieper"], 'subItem2', ["hello", "world", "Kiev", "Kharkiv", ["item1", ['subItem1', 'subItem2', 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']], "Odessa", "Lviv"], 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']]], "Lviv", "Dnieper"], 'subItem2', ["hello", "world", "Kiev", "Kharkiv", ["item1", ['subItem1', 'subItem2', 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']], "Odessa", "Lviv"], 'subItem3'], "item2", ['subItem4', 'subItem5', ['sub1sub', 'sub2sub', ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], 'sub3sub'], 'subItem6'], "item3", ['subItem7', 'subItem8', 'subItem9'], "item4", ['subItem10', 'subItem11', 'subItem12']]]

//____________ну і дефолтні із завдання__________________________

let arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]
let arr4 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
let arr5 = ["1", "2", "3", "sea", "user", 23];

createList(container, arr1_1)

//http://prison.kiev.ua/homework9_show-list-on-page_js/   на хостингу

// function setInt(s) {                               //цей таймер можна розкоментувати він сам створить див і після закінчення видалить його,
//     const element = document.createElement('div')            //він основному коду не заважає спочатку створив його, потім придумав рішення з рекурсією і воно мені більше сподобалося.
//     document.querySelector('.wrapper').appendChild(element)    //Якщо у когось із групи буде копія(з рекурсією) це моя, у телеграмі в нашій спільній групі викладав )))
//     element.classList.add('demo-2')                         //так що друга про всяк випадок
//     let start = new Date().getSeconds() + s + 1
//     const x = setInterval(function () {                       //до речі ця друга легко масштабується додаванням гетерів
//         const now = new Date().getSeconds()                   //(хвилини години.... роки) можна повноцінний таймер зробити тільки з мілісікунд треба буде перераховувати для кожного регістру
//         const movement = start - now
//         element.style.background = "rgb(67, 29, 29, 0.8)"
//         element.innerHTML = movement + " sec";
//         if (movement <= 0) {
//             clearInterval(x);
//             element.remove()
//         }
//     }, 1000);
// }
// setInt(5)



























