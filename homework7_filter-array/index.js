'use strict';
/*   Теоретичні питання
*
*   Опишіть своїми словами як працює метод forEach.
*       Як очистити масив?
*       Як можна перевірити, що та чи інша змінна є масивом?
*
*       Завдання
*       Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
*
*       Технічні вимоги:
*
*       Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
*       Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*
*       Література: http://learn.javascript.ru/array   http://learn.javascript.ru/array-iteration
*                   https://learn.javascript.ru/types-intro
*/

// 1 проходить по елементах масиву і застосовує функцію задану як аргумент forEach(callBack(item, index, array)-функція), до кожного елемента масиву один раз
// 2 splice(0) очищає масив та повертає значення видалених елементів
// 3 Array.isArray(myArray) перевіряє чи є елемент myArray масивом - повертає 'true' якщо масив і 'false' якщо ні

let myArray = [{name: 'xxx'}, 5, [1, 2, 3], true, 'car', 'Apple', false, 3, {name: 'Serhiy', lastname: NaN}, [2], '@', undefined];
let myArray2 = ['hello', 'world', 23, '23', null];

//****************** forEach ***************************

function filterBy(array, type) {
    let filterResultArray = [];
    array.forEach(function (key) {
        if ((typeof key) !== type)   //якщо тип перерахованого елемента не дорівнює переданому функції аргументу type
            /**continue; - не може бути використана дострокова зупинка**/
            filterResultArray.push(key); //пушемо елемент в кінець
    })
    return filterResultArray;
}

//****************** for ********************************

function filterBy2(array, type) {
    let filterResultArray = []
    for (let key of array) {              /**у звичайному циклі**/
        if ((typeof key) === type)      /**для наочності замість "нерівно", якщо 'true'**/
            continue;                 /**використав дострокове переривання ітерації**/
        filterResultArray.push(key);       //пушемо елемент в кінець в іншому випадку
    }
    return filterResultArray;
}

function filterBy3(array, type){                         //це дуже просто
  return  array.filter(key => (typeof key)  !== type);
}

console.log(filterBy3(myArray2, 'string'));
console.log(myArray2.filter(key => (typeof key)  !== 'string')); //без обгорткової функції ще простіше


console.log(filterBy(myArray2, 'string'));
console.log(filterBy2(myArray2, 'string'));
console.log(myArray);
console.log(filterBy(myArray, 'number'));
// console.log(filterBy(myArray,'boolean'));
// console.log(filterBy(myArray,'object'));

//****************************************************** end ***********************************************************

//прийоми які були перевірені в тестах при виконанні завдання

console.log(myArray[8].hasOwnProperty('lastname'));//true
console.log(11 in myArray);// true останній елемент.  якщо 12 - буде false
console.log(Array.isArray(myArray[2]));// true 2 елемент масив
console.log(Array.isArray(myArray[3]));// false 3 елемент не масив

