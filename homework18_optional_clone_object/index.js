'use strict';
const obj = {
    steck: 'js',
    user: {
        name: 'Vasiek',
        lastName: 'Limurov',
        'report card': {
            philosophy: 9,
            astronomy: '7',
            physics: 4,
        },
        container: [1, 2, '3', 'd'],
        user2: {
            name: 'Serhii',
            'last name': 'Petrov',
            'report card': {
                philosophy: '',
                astronomy: '',
                physics: ''
            }
        },
    },
    arrObj: [1, 2, {
        forest: 'wonderful',
        sea: 'frantically',
        sand: 'warm',
        UNILAD: {travel: '', adventure: '', discovery: ''},
        sun: 'marvelous',
        stones: 'great',
        sky: 'wonderful'
    }, '3', 'd'],
},arr = [{vehicle: {id: 1, name: "Luther"}},
    {
        steck:
            {
                gold: {
                    vehicle: {id: 2, name: "Luther King"},
                    guide: {id: 3, name: "Luther"}
                }
            }
    },
    {id: 4, name: "Петя"}, {id: 5, name: ""}, {id: 6, name: "Luther"}, {vehicle:[{id: 7, name: ""}, {
            id: 8,
            name: "Luther King"
        }]}
];

function cloneObj(obj) {
 let objClone = {};
    for (let j in obj) {
        if (typeof obj[j] === 'object') {
            for (let i in objClone) {
                if (i !== j) {
                    objClone[j] = obj[j];
                }
                cloneObj(obj[j]);
            }
        }
        objClone[j] = obj[j];
    }
    return objClone;
}

console.log(obj)
console.log(cloneObj(obj))
