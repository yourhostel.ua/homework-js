"use strict";
// Теоретичні питання
//
// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Які засоби оголошення функцій ви знаєте?
// Що таке hoisting, як він працює для змінних та функцій?
//
// Завдання
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// Технічні вимоги:
//
// Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
//
// Дата і час https://learn.javascript.ru/datetime
//**********************************************************************************************************************

// 1. Екранування це спосіб представлення спецсимволу як звичайного символу при побудові маски (патерну)
//      в регулярному вираженні за допомогою установки зворотного слеша, тим самим вказуючи інтерпретатору,
//           що спецсимвол повинен включатися в пошук як частина тексту.
//__________________________________________________________________________________________________________________________________
// 2. Оголошення функції function name(),  | функціональний вираз let variable = function(),  | () => expression стрілочна функція. |
//              завжди з ім'ям             |             може бути з ім'ям                    |              анонімна               |
//      викликати можна в будь-якому       |              або анонімна                        |                                     |
//            місці програми               |                      викликати можна за кодом нижче після оголошення                   |

// 3. hoisting(піднімати) представлений вище в "оголошенні функції" викликаючи функцію до оголошення не викличе помилку. Змінні треба оголошувати вище місць їх використання

function createNewUser() {                     //Створене в минулому завданні не коментував
    const newUser = {                          //Скопіював як є. Тут були додані сетер і два гетери вони описані
        firstName: undefined,
        lastName: undefined,
        birthday: undefined,
        setAskBlockName() {
            Object.defineProperty(this, 'firstName',
                {
                    value: prompt('Enter your first name'),
                    writable: false,
                    configurable: true,
                    enumerable: true
                }
            );
            Object.defineProperty(this, 'lastName',
                {
                    value: prompt('Enter your last name'),
                    writable: false,
                    configurable: true,
                    enumerable: false
                }
            );
        },
        setBirthDate() {                    //метод для перевірки введеної дати та додавання властивості birthday об'єкту newUser
            let formatDate;

            for (;;) {
                let date = prompt('enter date of birth: dd.mm.yyyy', 'dd.mm.yyyy')         //передаємо опитане значення змінної date
                formatDate = Date.parse((`${             //змінюємо послідовність, додаємо роздільники 
                    date.substring(6, 10)}-${            //тире для Date.parse(переводимо string у мілісекунди відповідної дати, ця передатна операція також далі допоможе зробити валідацію на 'Invalid Date')
                    date.substring(3, 5)}-${                                              //за допомогою регулярного виразу, створюємо патерн(маску) для валідації 
                    date.substring(0, 2)}`));                                             //формату дати введеного користувачем заданого як dd.mm.yyyy. Обмежуємо вираз якорями ^ і $
                if (date.match(/^\d{2}[.]\d{2}[.]\d{4}$/) && formatDate < Date.now()) {  //паралельно перевіряємо, щоб рік народження не був більшим ніж сьогоднішній та перевіряємо на 'Invalid Date' 13 місяців або 32 число
                    alert('Successfully!')                                                 //якщо обидві умови true повідомляємо про успіх
                    break                                                                  //виходимо з циклу
                } else if (!(confirm('The date format is invalid. Will repeat?'))) break   //якщо ні, повідомляємо про невірний формат і запитуємо "Повторити?"
            }                                                                              //Якщо користувач натиснув скасувати завершуємо цикл break
            const newDate = new Date(formatDate); //Мілісекунди у формат дати              //Якщо Ok повторюємо спробу. І так доти, поки користувач не натисне скасування або не введе правильні дані
            Object.defineProperty(this, 'birthday',                                     //додаємо дату властивості birthday об'єкту newUser
                {
                    value: newDate,
                }
            );
        },
        setFirstName(newFName) {
            Object.defineProperty(this, 'lastName',
                {
                    value: newFName
                }
            )
        },
        setLastName(newLName) {
            Object.defineProperty(this, 'firstName',
                {
                    value: newLName
                }
            )
        },
        getAge() {   //Метод повертає вік користувача
            const nowDate = new Date();
            const nowDateCutTime = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()); //дата сьогодні без часу
            const dateOfBirthNow = new Date(nowDateCutTime.getFullYear(), newUser.birthday.getMonth(), newUser.birthday.getDate());//цьогорічний день народження
            let years;                                                                                                                //вік
            years = nowDateCutTime.getFullYear() - newUser.birthday.getFullYear(); //різниця: дата сьогодні мінус дата народження
            if (nowDateCutTime < dateOfBirthNow) {                                 //якщо дня народження цього року не було віднімаємо рік
                years = years - 1;
            }
            console.log(years)
        },
        getPassword() {    //метод повертає пароль, створений з першої літери імені у верхньому регістрі + прізвище + 'yyyy'
            return console.log(`Your password - ${         //повертаємо пароль   
                newUser.firstName[1].toUpperCase()}${     // Перша літера імені у верхньому регістрі
                newUser.lastName}${                      //прізвище як є
                newUser.birthday.getFullYear()}`);      //Вичленовуємо рік із дати народження користувача 'yyyy'
        },
    };

    newUser.setAskBlockName();
    newUser.setBirthDate();
    return newUser
}

const newUser = createNewUser()

console.log(newUser)     //перевірка

newUser.getAge()        //вік користувача

newUser.getPassword()   //пароль користувача





