"use strict";

function drawCircle() {

    let wrapper

    function genButtonStart() {
        wrapper = document.createElement('div')
        document.body.appendChild(wrapper).classList.add('wrapper')
        wrapper.insertAdjacentHTML("afterbegin", '<button class="bt1">намалювати коло</button>')
    }

    function generateFigure(elementSize, tic, time) {         //блок генерації куль
        function tick(i) {                                     //блок швидкості викиду куль
            let idTimeout = setTimeout(function () {
                tick(i - 1);                                  //рекурсією пригальмовуємо
                document.querySelector('.wrapper').insertAdjacentHTML("afterbegin",
                    `<div class ='circle' style="width:${elementSize}px;height:${elementSize}px; 
               background-color: #${(Math.random().toString(16) + '000000').substring(2, 8).toUpperCase()}"></div>`)
            }, time)                                            //на задане значення
            if (i < 0) clearTimeout(idTimeout)
        }
        tick(tic)
    }

    function genOption(select, start, amount, stepLength, unit = '') {
        for (let i = start; i <= amount; i += stepLength) {
            const option = document.createElement('option')
            select.append(option)
            option.value = `${i}`
            option.textContent = `${i} ` + unit
        }
    }

    function gen() {
        document.querySelector('.bt1').onclick = function () {
            document.querySelector('.bt1').remove() //стираємо кнопку "намалювати коло" після її натискання

//********************************* і починаємо генерувати форму введення **********************************************

            wrapper.insertAdjacentHTML("afterbegin", '<form class="container"></form>')

            const container = document.querySelector('.container')

            container.insertAdjacentHTML("afterbegin", '<span class="span">вибери параметри</span>')
            container.insertAdjacentHTML("beforeend", '<button class="bt2 select">насипати куль</button>')

//************************************* генеруємо select-и *************************************************************

            for (let i = 1; i <= 3; i++) {
                const select = document.createElement('select')
                container.insertBefore(select, document.querySelector('.bt2'))
                select.classList.add('select')
                select.setAttribute(`id`, `select${i}`)
            }

//************************************* генеруємо для select-ов label-и ************************************************

            for (let i = 1; i <= 3; i++) {
                const label = document.createElement('label')
                container.insertBefore(label, document.getElementById(`select${i}`))
                label.setAttribute(`for`, `select${i}`)
                label.classList.add(`label${i}`)
            }

            document.querySelector('.label1').textContent = "діаметр кулі"
            document.querySelector('.label2').textContent = "Кількість куль"
            document.querySelector('.label3').textContent = "подавати кулю кожні"

//************************************* генеруємо для select-ов опції **************************************************

            const select1 = document.getElementById('select1')
            genOption(select1, 5, 100, 5, 'px')

            const select2 = document.getElementById('select2')
            genOption(select2, 5, 100, 5, 'шт')

            const select3 = document.getElementById('select3')
            genOption(select3, 100, 1000, 100, 'ms')

//********************************* зчитуємо по кліку select-и *********************************************************

            let a = 20, b = 5, c = 100;              //якщо не клацали даємо замовчування

            document.querySelector('form').addEventListener('click', () => {
                a = select1.value                    //якщо клацали присвоюємо обрані опції
                b = select2.value
                c = select3.value
            })

            document.querySelector('.bt2').addEventListener('click', function () { //за кліком на кнопку форми
                document.querySelector('.container').remove() //видаляємо форму
                wrapper.style.borderRadius = a * 0.5 + 'px'         //задаем радиус border-а wrapper-а равным радиусу шара
                generateFigure(a, b, c)                           // передаємо параметри вибрані(або за замовчуванням)
            })                                                    //користувачем генератору куль на виконання

            wrapper.addEventListener('click', function (event) {
                if (event.target.classList.contains('circle')) {       //якщо подія на кулі
                    event.target.remove()                       //видаляємо кулю
                }
            })
        }
    }//********************************* кінець функції gen ************************************************************

    document.body.addEventListener('click', () => {  // по кліку починаємо
        console.log(document.body.childNodes.length)
        if (!document.querySelector('.wrapper')) { //умова при якому клік не спрацює під час виконання функції gen()
            genButtonStart()                  //створюємо по кліку кнопку старт 'намалювати коло'
            gen()                           //запускаємо основну функцію
        }
    })

    document.body.addEventListener('dblclick', () => {      //Умова при якому дабл кліком можна викликати форму заново щоб досипати куль,
        if (document.querySelectorAll('.circle').length > 0 && !document.querySelector('.bt1')){  //спрацює тільки в тому випадку якщо кулі вже є.
        wrapper.insertAdjacentHTML("afterbegin", '<button class="bt1">намалювати коло</button>')      //і немає кнопки виклику форми
      gen()
        }
    })
}

drawCircle()

/*http://prison.kiev.ua/homework21_optional_draw_circle/*/












