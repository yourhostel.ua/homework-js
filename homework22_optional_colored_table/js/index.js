"use strict";

function genTable() {
    document.body.innerHTML = '<div class="container">' +
        '<h1 class="caption">Homework22 optional colored table</h1><table class="table">' +
        '</table></div>'

    const container = document.querySelector('.container')
    const table = document.querySelector('table')

    function createTable() {
        let i = 1
        for (; i < 31; i++) {
            const tr = document.createElement('tr')
            table.appendChild(tr).classList.add('tr')
            for (let j = 0; j < 30; j++) {
                let td = document.createElement('td')
                td.classList.add('td')
                document.querySelector(`.tr:nth-child(${i})`).append(td)
            }
        }
    }

    createTable()

    table.addEventListener('click', (event) => {
            event.target.classList.toggle('black')
    })

    container.addEventListener('click', (event) => {
        if (event.target === container) {
            document.querySelectorAll('td').forEach((element) => {
                element.classList.remove('black')
            })
        }
    })
}

genTable()






