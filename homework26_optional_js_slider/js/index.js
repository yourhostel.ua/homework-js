'use strict';
function getPic(i){return document.querySelector(`[src$='${i}.jpg']`)}
function setPic(i){
    let wrapper = document.querySelector('.wrapper')
        wrapper.insertAdjacentHTML('afterbegin',`<img class = "images-wrapper" src='./images/${i}.jpg' alt="">`)
}

let j = 1
for(let i = -900; i <= 450; i = i + 450){   //задаємо початкове становище всіх картинок
    getPic(j).style.left = `${i}` + 'px'
    j++
}

let isAnimating = false;

document.querySelector('.container').addEventListener('click', (e) => {
    if (isAnimating) return;
    isAnimating = true;
    if(e.target.closest('.next')){
        for(let i = 1; i <= 4; i++){
        let f = getPic(i).style.left.replace('px','')
            getPic(i).style.left =  450 + +f + 'px'  //при натисканні на next додаємо всім зміщення на ширину картинки вправо
            if(+f === 900){
                setTimeout(() =>{
                    getPic(i).remove()
                    setPic(i)
                    getPic(i).style.left = -450 + 'px'  //якщо картинка, що перевіряється, досягла краю, переміщуємо її на початок
                },1000,i)
            }
        }
    } else if(e.target.closest('.previous')){
        for(let i = 1; i <= 4; i++){
            let f = getPic(i).style.left.replace('px','')
            getPic(i).style.left =  +f -450 + 'px'  //при натисканні на previous додаємо всім зміщення на ширину картинки вліво
            if(+f === -900){
                setTimeout(() =>{
                    getPic(i).remove()
                    setPic(i)
                    getPic(i).style.left = 450 + 'px' //якщо картинка, що перевіряється, досягла краю, переміщуємо її в кінець
                },1000,i)
            }
        }
    }

    setTimeout(() => isAnimating = false,1000); // Скидання прапора після завершення анімації,
})


