'use strict';

const arr = [
    {
        vehicle: {
            id: 1, name: "Маша"
        }
    },
    {
        stack: {
            guide: {
                id: 2, name: "Глеб Петрович",
            },
            vehicle: {
                militant: {
                    id: 3, name: "изя изя"
                },
            }
        }
    },
    {
        user1: {
            id: 4, name: "изя"
        }
    },
    {
        user2: {
            id: 5, name: "изя"
        }
    },
    {
        user3: {
            id: 6, name: "Julius Caesar"
        }
    },
    {
        name: "auto"
    },
    {
        name: "Caesar"
    },
    {
        name: "auto Toyota"
    },
];

function filterCollection(arr, str = '', bool = true, ...field) {
    let result = [],
      fieldArr = [];

str = str.toUpperCase()

             filter()
    function filter() {
        for (let k = 0; k < field.length; k++) {
            arr.forEach((e) => {                 //створюємо масив масивів-шляхів
                if(bool && typeof e[field[k].split('.')] === "string"){//для першого рівня вкладеності
                    if( e[field[k].split('.')].toUpperCase() === str ) { //додаємо до результатів пошуку з точним збігом
                        result.push(e[field[k].split('.')])                   //виключаємо з масиву шляхів
                    }
                }else if(!bool && typeof e[field[k].split('.')] === "string"){//для першого рівня вкладеності
                    if( e[field[k].split('.')].toUpperCase().includes(str) ) { //якщо шукане містить запит як частину
                        result.push(e[field[k].split('.')])                       //виключаємо з масиву шляхів
                    }
                }else if (e[field[k].split('.')[0]]) {  // не додаємо не існуючі(при першому входженні) шляхи якщо вони є у запиті
                    fieldArr.push(field[k].split('.'))
                }
            })
        }
    }

    console.log(fieldArr)

    function* gen() {          // генератор видає по одній папці за раз
        for (let k = 0; k < fieldArr.length; k++) {
            for (let l = 0; l < fieldArr[k].length; l++)
                yield fieldArr[k][l]
        }
    }

    let prom = gen(), fn

    function search(obj) {
        fn = prom.next().value             //отримуємо з генератора папку
        for (const j in obj) {                                  //обходимо масив шукаючи папку
            if (bool && typeof obj[j][fn] === 'string' && !Array.isArray(obj)) {
                if( obj[j][fn].toUpperCase() === str ) {              //додаємо до результатів пошуку з точним збігом
                    result.push(obj[j][fn])
                }
            }
            if (!bool && typeof obj[j][fn] === 'string' && !Array.isArray(obj)){
                if( obj[j][fn].toUpperCase().includes(str)) {            //якщо шукане містить запит як частину
                    result.push(obj[j][fn])
                }
            }
            if (typeof obj[j][fn] === 'object') {
                search(obj[j])                                        //йдемо глибше
            }
        }
    }

    for (let i = 0; i < fieldArr.length; i++) {  //запускаємо для кожного шляху
        search(arr)
    }

// function call(i){    //перевірка генератора
//     console.log(prom.next().value)
//     if(i >= fieldArr.reduce((sum,b)=>sum + b.length,0)) return
//     setTimeout(()=>{call(i + 1)},500)
// }
// call(1)
    console.log(result)
}

filterCollection(arr, 'caesar', false,'name', 'stack.vehicle.militant.name', 'stack.guide.name', 'vehicle.name',
    'user2.name', 'user1.name', 'user6.name'/*немає в arr - ігнорується*/)
console.log('----------------------------------------------------')
